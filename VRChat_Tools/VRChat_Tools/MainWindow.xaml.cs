﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VRChat_Tools.CheatFolder;

namespace VRChat_Tools
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Properties

        private CheatCounter cheatCounter;

        #endregion


        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
            InitializeCheat();
            InitializeMike();
        }

        #endregion

        #region Methods

        private void InitializeMike()
        {
            Label_Counter.Content = "0";
        }

        private void On_Buton_Click(object sender, RoutedEventArgs e)
        {
            if (Label_Counter.Content.GetType() == typeof(string))
            {
                Label_Counter.Content = int.Parse((string)Label_Counter.Content) + 1;
            }
            else if (Label_Counter.Content.GetType() == typeof(int))
            {
                Label_Counter.Content = (int)Label_Counter.Content + 1;
            }
        }

        #endregion

        #region Cheat
        private void InitializeCheat()
        {
            cheatCounter = new CheatCounter(0);
        }

        private void Button_Cheat_Click(object sender, RoutedEventArgs e)
        {
            Label_Counter.Content = $"Mon super compteur est égal à : {cheatCounter.Counter}";
            cheatCounter.Increment();
        } 

        #endregion
    }
}
//000...1 = 1 (base 10) pour un unsigned int