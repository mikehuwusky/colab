﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRChat_Tools.CheatFolder
{
    internal class CheatCounter
    {
        #region Constructor

        public CheatCounter(int counter)
        {
            Counter = counter;
        } 

        #endregion

        #region Property

        internal int Counter { get; private set; }

        #endregion+

        #region Methods

        internal void Increment()
        {
            Counter = Counter + 1; // Ou Counter++;
        }

        #endregion
    }
}
